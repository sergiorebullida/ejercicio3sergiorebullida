package ejercicio3;

import java.util.Scanner;

public class Ejercicio3 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);

		System.out.println("Introduce una cadena:");
		String cadena = input.nextLine().toLowerCase();
		
		System.out.println(cadena);

		input.close();

	}

}
